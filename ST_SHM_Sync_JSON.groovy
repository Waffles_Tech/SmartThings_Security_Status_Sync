/**
 * 
 * Original code was from rleonard55, and can be found at:
 * https://github.com/rleonard55/rleonard-SmartThings/blob/master/smartapps/rleonard55/sync-smart-home-monitor-and-switch.src/sync-smart-home-monitor-and-switch.groovy
 * 
 * I modified the code to add an API and JSON even to retrieve the status of the SmartThings Smart Home Monitor for use in things, such as my other script, which lights an LED Depending on the status of the SHM
 * 
 * Example Curl Command (linux) to retreive JSON status: 
 * curl -H "Authorization: Bearer API-TOKEN" "URL/API/ENDPOINT"
 *
 */
 
definition(
    name: "Sync ST Switch and SHM with JSON call",
    namespace: "waffles",
    author: "Waffles",
    description: "Links the status of the Smart Home Monitor to a switch and creates a retrievable JSON call",
    iconUrl: "https://cdn4.iconfinder.com/data/icons/materia-security-vol-3/24/022_110_crypto_switch_commutator_security_lock_arrows-256.png",
    iconX2Url: "https://cdn4.iconfinder.com/data/icons/materia-security-vol-3/24/022_110_crypto_switch_commutator_security_lock_arrows-256.png",
    iconX3Url: "https://cdn4.iconfinder.com/data/icons/materia-security-vol-3/24/022_110_crypto_switch_commutator_security_lock_arrows-256.png")

preferences {
	page(name:"SettingsPage")
}

mappings {
  path("/alarmStatus") {
    action: [
      GET: "queryStatus"
    ]
  }
}

def SettingsPage(){
	dynamicPage(name:"SettingsPage", install:true, uninstall:true){
        section("Sync this switch with the Smart Home Monitor") {
            input("MySwitch", "capability.switch", title: "Switches to Sync", multiple: false, required: true)
        }
        section("Use presence sensor(s) to determin arm mode", hideWhenEmpty: true){
            input ("presenceSensors", "capability.presenceSensor", title: "Presence Sensor(s)", multiple: true, required: false,submitOnChange: true)
        }
        section("No presence sensors -> Set default arm mode.", hideWhenEmpty: true){
            if(presenceSensors == null)
            	input("defaultArmMode", "enum", title: "Default Arm Mode", options: ["away", "stay"], defaultValue: "away")
        }   
        section("Optional: Run routines on arm/disarm", hideWhenEmpty: true, hideable: true, hidden: true){
            def phrases = location.helloHome?.getPhrases()*.label
            if (phrases) {
                phrases.sort()
                input (name:"armRoutine",type: "enum", title: "Arm Away Routine", options: phrases, required:false)
                input (name:"stayRoutine",type: "enum", title: "Stay Routine", options: phrases, required:false)
                input (name:"disarmRoutine",type: "enum", title: "Disarm Routine", options: phrases, required:false)
            }
        }
    }
}

def installed() {
	log.debug "Installed with settings: ${settings}"
	initialize()
}
def updated() {
	log.debug "Updated with settings: ${settings}"

	unsubscribe()
	initialize()
}
def initialize() {
	subscribeIt()
    state.securityStatus = false
}

def subscribeIt(){
    subscribe(location,"alarmSystemStatus",alarmStatusHandler)
    subscribe(MySwitch, "switch.on", switchOnHandler)
    subscribe(MySwitch, "switch.off", switchOffHandler)
}

def alarmStatusHandler(event) {
	log.debug "Caught alarm status change: "+event.value
    try{
        unsubscribe()

		if (event.value == "off") {
        MySwitch.off()
        state.securityStatus = false
        } else if (event.value == "away") {
        MySwitch.on()
        state.securityStatus = true
        } else if (event.value == "stay") {
        MySwitch.on()
        state.securityStatus = true
        }
        
	} catch(all){
        log.error all
    }
    subscribeIt()
}

def switchOnHandler (evt) {
	log.debug "Switch on handler raised"
    state.securityStatus = true
    try{
        unsubscribe()

        def armMode = 'stay'

        if(presenceSensors == null)
            armMode = defaultArmMode
        else if(!anyonePresent())
            armMode = 'away'

        sendSHMEvent(armMode)
        execRoutine(armMode)
    } catch(all){
    	log.error all
    }
	subscribeIt()
}
def switchOffHandler (evt) {
	log.debug "Switch off handler raised"
    state.securityStatus = false
    try {
        def armMode = 'off'
        unsubscribe()

        sendSHMEvent(armMode)
        execRoutine(armMode)
    } catch(all){
        log.error all
    }
    subscribeIt()
}

def queryStatus() {
    return [state.securityStatus]
}

private sendSHMEvent(String shmState){
	def event = [name:"alarmSystemStatus", value: shmState, 
    			displayed: true, description: "System Status is ${shmState}"]
    sendLocationEvent(event)
}
private execRoutine(armMode) {
	if (armMode == 'away') location.helloHome?.execute(settings.armRoutine)
    else if (armMode == 'stay') location.helloHome?.execute(settings.stayRoutine)
    else if (armMode == 'off') location.helloHome?.execute(settings.disarmRoutine)    
}
private anyonePresent() {
	return presenceSensors.any {p -> p.currentValue("presence") == "present" }
}